﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationFigure
{
    public interface IVisitor
    {
        string Serialize(XmlSerialization xmlSerialization);
        string Serialize(JsonSerialization jsonSerialization);
    }
}
