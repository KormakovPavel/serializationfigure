﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace SerializationFigure
{
    class PointFigure : IVisitor
    {
        private readonly int x;
        private readonly int y;

        public PointFigure(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public string Serialize(XmlSerialization xmlSerialization)
        {
            StringBuilder xmlContent = new StringBuilder("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>\r\n");
            xmlContent.Append("<Point>\r\n");
            xmlContent.Append($"<X>{x}</X>\r\n");
            xmlContent.Append($"<Y>{y}</Y>\r\n");
            xmlContent.Append("</Point>");

            return $"Точка сериализована в XML:\n{xmlContent.ToString()}";
        }

        public string Serialize(JsonSerialization jsonSerialization)
        {            
            string jsonContent = "{ \"Figure\":\"Point\",\"X\": " + x + ",\"Y\": " + y + "}";            

            return $"Точка сериализована в JSON:\n{jsonContent}";
        }
    }
}
