﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace SerializationFigure
{
    class Circle : IVisitor
    {
        private readonly int radius;

        public Circle(int _radius)
        {
            radius = _radius;
        }
        public string Serialize(XmlSerialization xmlSerialization)
        {
            StringBuilder xmlContent = new StringBuilder("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>\r\n");
            xmlContent.Append("<Circle>\r\n");
            xmlContent.Append($"<Radius>{radius}</Radius>\r\n");
            xmlContent.Append("</Circle>");
                       
            return $"Круг сериализован в XML:\n{xmlContent.ToString()}";
        }

        public string Serialize(JsonSerialization jsonSerialization)
        {   
            string jsonContent = "{ \"Figure\":\"Circle\",\"Radius\": " + radius + "}";
            
            return $"Круг сериализован в JSON:\n{jsonContent}";
        }
    }
}
