﻿using System;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace SerializationFigure
{
    class Square : IVisitor
    {
        private readonly int width;

        public Square(int _width)
        {
            width = _width;
        }

        public string Serialize(XmlSerialization xmlSerialization)
        {
            StringBuilder xmlContent = new StringBuilder("<? xml version = \"1.0\" encoding = \"utf - 8\" ?>\r\n");
            xmlContent.Append("<Square>\r\n");
            xmlContent.Append($"<Width>{width}</Width>\r\n");            
            xmlContent.Append("</Square>");

            return $"Квадрат сериализован в XML:\n{xmlContent.ToString()}";
        }

        public string Serialize(JsonSerialization jsonSerialization)
        {            
            string jsonContent = "{ \"Figure\":\"Square\",\"Width\": "+ width + "}";
            
            return $"Квадрат сериализован в JSON:\n{jsonContent}";
        }
    }
}
