﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationFigure
{
    public class Context
    {
        private readonly ISerialization serialization;
        private readonly IVisitor visitor;

        public Context(IVisitor _visitor, ISerialization _serialization)
        {
            serialization = _serialization;
            visitor = _visitor;
        }

        public string Serialize()
        {
           return serialization.Serialize(visitor);
        }
    }
}
