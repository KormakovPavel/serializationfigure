﻿using System;

namespace SerializationFigure
{
    class Program
    {
        static void Main()
        {
            var pointFigure = new PointFigure(5,10);
            var circle = new Circle(15);
            var square = new Square(20);
            var xmlSerialization = new XmlSerialization();
            var jsonSerialization = new JsonSerialization();

            var pointXml = new Context(pointFigure, xmlSerialization);
            var pointJson = new Context(pointFigure, jsonSerialization);
            var circleXml = new Context(circle, xmlSerialization);
            var circleJson = new Context(circle, jsonSerialization);
            var squareXml = new Context(square, xmlSerialization);
            var squareJson = new Context(square, jsonSerialization);

            Console.WriteLine(pointXml.Serialize());
            Console.WriteLine(pointJson.Serialize());
            Console.WriteLine(circleXml.Serialize());
            Console.WriteLine(circleJson.Serialize());
            Console.WriteLine(squareXml.Serialize());
            Console.WriteLine(squareJson.Serialize());

            Console.ReadLine();
        }
    }
}
