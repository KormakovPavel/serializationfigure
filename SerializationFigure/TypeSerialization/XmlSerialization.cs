﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationFigure
{
    public class XmlSerialization : ISerialization
    {
        public string Serialize(IVisitor visitor)
        {
           return visitor.Serialize(this);
        }
    }
}
