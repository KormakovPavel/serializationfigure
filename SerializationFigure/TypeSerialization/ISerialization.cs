﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationFigure
{
    public interface ISerialization
    {
        string Serialize(IVisitor visitor);
    }
}
